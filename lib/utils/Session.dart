import 'package:menuku/Model/AccessToken.dart';
import 'package:menuku/Model/UserInformation.dart';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';


AccessToken ?accessToken;

setAccessToken(AccessToken accessToken) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String accessTokenString = json.encode(accessToken.toJson());
  prefs.setString('atSession', accessTokenString);
}

Future<AccessToken?> getAccessToken() async{
  AccessToken accessToken;
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String? accessTokenString = (await prefs.get('atSession')) as String?;
  if(accessTokenString == null){

  }else{
    accessToken  = AccessToken.fromJson(json.decode(accessTokenString));
    return accessToken;

  }
}

setUserInformation(UserInformation userInformation) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String userInformationString = json.encode(userInformation);
  prefs.setString('userInformation', userInformationString);
}

Future<bool> destroyUserInformation() async{
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return preferences.setString('userInformation', '');
}

Future<bool> destroyAccessToken() async{
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return preferences.setString('atSession', '');
}

Future<UserInformation?> getUserInformation() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String? userInformationString = (await prefs.get('userInformation')) as String?;
  if(userInformationString==null || userInformationString == ''){
    return null;
  }
  UserInformation? userInformation = UserInformation.fromJson(json.decode(userInformationString));
  print("userinfo " + userInformation.user.userClientCode);
  return userInformation;
}

setUserSession() async {

}

Future<bool> checkSession() async {
  SharedPreferences checkSession = await SharedPreferences.getInstance();
  var user = checkSession.getString('userInformation');
  if (user != null ){
    return false;
  } else {
    return true;
  }
}