
import 'package:flutter/material.dart';

TextStyle Inter24Bold = TextStyle(
    fontSize: 24,
    fontFamily: 'Inter-SemiBold',
    fontWeight: FontWeight.normal,
    color: Colors.black
);
TextStyle InterRegular14 = TextStyle(
    fontSize: 14,
    fontFamily: 'Inter-Regular',
    fontWeight: FontWeight.normal,
    color: Colors.black
);
TextStyle InterBold14 = TextStyle(
    fontSize: 14,
    fontFamily: 'Inter-Regular',
    fontWeight: FontWeight.normal,
    color: Colors.white
);