// To parse this JSON data, do
//
//     final getCartModel = getCartModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

class GetCartModel {
  GetCartModel({
    required this.menusRestoCode,
    required this.name,
    required this.price,
    required this.priceAfterDisc,
    required this.desc,
    required this.discPercent,
    required this.discAmount,
    required this.pictures,
    required this.qty,
  });

  String menusRestoCode;
  String name;
  String price;
  String priceAfterDisc;
  String desc;
  String discPercent;
  String discAmount;
  String pictures;
  String qty;

  factory GetCartModel.fromRawJson(String str) => GetCartModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GetCartModel.fromJson(Map<String, dynamic> json) => GetCartModel(
    menusRestoCode: json["menus_resto_code"],
    name: json["name"],
    price: json["price"],
    priceAfterDisc: json["price_after_disc"],
    desc: json["desc"],
    discPercent: json["disc_percent"],
    discAmount: json["disc_amount"],
    pictures: json["pictures"],
    qty: json["qty"],
  );

  Map<String, dynamic> toJson() => {
    "menus_resto_code": menusRestoCode,
    "name": name,
    "price": price,
    "price_after_disc": priceAfterDisc,
    "desc": desc,
    "disc_percent": discPercent,
    "disc_amount": discAmount,
    "pictures": pictures,
    "qty": qty,
  };
}
