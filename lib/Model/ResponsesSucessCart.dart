// To parse this JSON data, do
//
//     final responseSuccesCart = responseSuccesCartFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

class ResponseSuccesCart {
  ResponseSuccesCart({
    required this.status,
    required this.mssg,
  });

  int status;
  String mssg;

  factory ResponseSuccesCart.fromRawJson(String str) => ResponseSuccesCart.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ResponseSuccesCart.fromJson(Map<String, dynamic> json) => ResponseSuccesCart(
    status: json["status"],
    mssg: json["mssg"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "mssg": mssg,
  };
}
