// To parse this JSON data, do
//
//     final userInformation = userInformationFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

class UserInformation {
  UserInformation({
    required this.status,
    required this.user,
    required this.mssg,
  });

  int status;
  User user;
  String mssg;

  factory UserInformation.fromRawJson(String str) => UserInformation.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory UserInformation.fromJson(Map<String, dynamic> json) => UserInformation(
    status: json["status"],
    user: User.fromJson(json["user"]),
    mssg: json["mssg"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "user": user.toJson(),
    "mssg": mssg,
  };
}

class User {
  User({
    required this.id,
    required this.userClientCode,
    required this.username,
    required this.createdAt,
    required this.updatedAt,
    required this.email,
  });

  int id;
  String userClientCode;
  String username;
  DateTime createdAt;
  dynamic updatedAt;
  String email;

  factory User.fromRawJson(String str) => User.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    userClientCode: json["user_client_code"],
    username: json["username"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"],
    email: json["email"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_client_code": userClientCode,
    "username": username,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt,
    "email": email,
  };
}
