// To parse this JSON data, do
//
//     final menuModel = menuModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

class MenuModel {
  MenuModel({
    required this.status,
    required this.data,
  });

  int status;
  List<Datum> data;

  factory MenuModel.fromRawJson(String str) => MenuModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MenuModel.fromJson(Map<String, dynamic> json) => MenuModel(
    status: json["status"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    required this.menusRestoCode,
    required this.name,
    required this.price,
    required this.priceAfterDisc,
    required this.desc,
    required this.discPercent,
    required this.discAmount,
    required this.pictures,
  });

  String menusRestoCode;
  String name;
  String price;
  String priceAfterDisc;
  String desc;
  String discPercent;
  String discAmount;
  String pictures;

  factory Datum.fromRawJson(String str) => Datum.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    menusRestoCode: json["menus_resto_code"],
    name: json["name"],
    price: json["price"],
    priceAfterDisc: json["price_after_disc"],
    desc: json["desc"],
    discPercent: json["disc_percent"],
    discAmount: json["disc_amount"],
    pictures: json["pictures"],
  );

  Map<String, dynamic> toJson() => {
    "menus_resto_code": menusRestoCode,
    "name": name,
    "price": price,
    "price_after_disc": priceAfterDisc,
    "desc": desc,
    "disc_percent": discPercent,
    "disc_amount": discAmount,
    "pictures": pictures,
  };
}
