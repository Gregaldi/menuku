// To parse this JSON data, do
//
//     final getRestoModel = getRestoModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

class GetRestoModel {
  GetRestoModel({
    required this.status,
    required this.data,
  });

  int status;
  List<Datum> data;

  factory GetRestoModel.fromRawJson(String str) => GetRestoModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GetRestoModel.fromJson(Map<String, dynamic> json) => GetRestoModel(
    status: json["status"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    required this.restoCode,
    required this.userRestoCode,
    required this.name,
    required this.address,
    required this.noTelp,
    required this.long,
    required this.lat,
    required this.dineIn,
    required this.takeAway,
    required this.status,
    required this.rating,
    required this.pictures,
    required this.category,
  });

  String restoCode;
  String userRestoCode;
  String name;
  String address;
  String noTelp;
  String long;
  String lat;
  String dineIn;
  String takeAway;
  String status;
  String rating;
  String pictures;
  List<Category> category;

  factory Datum.fromRawJson(String str) => Datum.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    restoCode: json["resto_code"],
    userRestoCode: json["user_resto_code"],
    name: json["name"],
    address: json["address"],
    noTelp: json["no_telp"],
    long: json["long"],
    lat: json["lat"],
    dineIn: json["dine_in"],
    takeAway: json["take_away"],
    status: json["status"],
    rating: json["rating"],
    pictures: json["pictures"],
    category: List<Category>.from(json["category"].map((x) => Category.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "resto_code": restoCode,
    "user_resto_code": userRestoCode,
    "name": name,
    "address": address,
    "no_telp": noTelp,
    "long": long,
    "lat": lat,
    "dine_in": dineIn,
    "take_away": takeAway,
    "status": status,
    "rating": rating,
    "pictures": pictures,
    "category": List<dynamic>.from(category.map((x) => x.toJson())),
  };
}

class Category {
  Category({
    required this.code,
    required this.desc,
  });

  String code;
  String desc;

  factory Category.fromRawJson(String str) => Category.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    code: json["code"],
    desc: json["desc"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "desc": desc,
  };
}
