// To parse this JSON data, do
//
//     final userInformationError = userInformationErrorFromJson(jsonString);

import 'dart:convert';

UserInformationError userInformationErrorFromJson(String str) => UserInformationError.fromJson(json.decode(str));

String userInformationErrorToJson(UserInformationError data) => json.encode(data.toJson());

class UserInformationError {
  UserInformationError({
    required this.error,
    required this.errorMsg,
  });

  bool error;
  String errorMsg;

  factory UserInformationError.fromJson(Map<dynamic, dynamic> json) => UserInformationError(
    error: json["error"],
    errorMsg: json["error_msg"],
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "error_msg": errorMsg,
  };
}
