import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:menuku/Users/RootAppMenukuScanBarcode.dart';
import 'package:menuku/Users/root_app.dart';
import 'package:menuku/utils/colors.dart';

class Button extends StatefulWidget {
  const Button({Key? key}) : super(key: key);

  @override
  _ButtonState createState() => _ButtonState();
}

class _ButtonState extends State<Button> {

  Widget getFooter() {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height*0.25,
      width: size.width,
      child: Padding(
        padding: const EdgeInsets.only(top: 15),
        child: Column(
          children: [
            Container(
                margin: EdgeInsets.only(top: 2,bottom: 10),
                height:size.height*0.1,
                width: MediaQuery.of(context).size.width*0.9,
              decoration: BoxDecoration(
                  color: white,
                  border: Border(
                      top: BorderSide(color: black.withOpacity(0.1)),
                      bottom: BorderSide(color: black.withOpacity(0.1)),
                      left: BorderSide(color: black.withOpacity(0.1)),
                      right: BorderSide(color: black.withOpacity(0.1))
                  )),
                child:
                Center(
                  child:
                  ListTile(
                    trailing:Icon(Icons.arrow_forward_ios),
                    title: Text("Scan QR code",style: TextStyle(fontSize: 16,color: Colors.green,fontWeight: FontWeight.w600,),),
                    leading:  SvgPicture.asset(
                      "assets/images/scan.svg",
                      width: 50,
                      color:  Colors.green,
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => RootApp1()),
                      );
                    },
                  ),
                )
            ),
            Container(
                margin: EdgeInsets.only(top: 2,bottom: 10),
                height:size.height*0.1,
                width: MediaQuery.of(context).size.width*0.9,
                decoration: BoxDecoration(
                    color: white,
                    border: Border(
                        top: BorderSide(color: black.withOpacity(0.1)),
                        bottom: BorderSide(color: black.withOpacity(0.1)),
                        left: BorderSide(color: black.withOpacity(0.1)),
                        right: BorderSide(color: black.withOpacity(0.1))
                    )),
                child:
                Center(
                  child:
                  ListTile(
                    trailing:Icon(Icons.arrow_forward_ios),
                    title: Text("Cari Resto",style: TextStyle(fontSize: 16,color: Colors.green,fontWeight: FontWeight.w600,),),
                    leading:  Icon(Icons.find_in_page_outlined,color: Colors.green, size: 50,),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => RootApp()),
                      );
                    },
                  ),
                )
            ),


          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomSheet:getFooter()


    );
  }
}
