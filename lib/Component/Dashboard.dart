import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:menuku/utils/colors.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {

    Widget getFooter() {
      var size = MediaQuery.of(context).size;
      return Container(
        height: size.height*0.15,
        width: size.width,
        child: Padding(
          padding: const EdgeInsets.only(top: 15,left: 15,right: 15),
          child:
          Container(
              margin: EdgeInsets.only(top: 2,bottom: 10),
              height:size.height*0.1,
              width: MediaQuery.of(context).size.width*0.9,
              decoration: BoxDecoration(
                  color: white,
                  border: Border(
                      top: BorderSide(color: black.withOpacity(0.1)),
                      bottom: BorderSide(color: black.withOpacity(0.1)),
                      left: BorderSide(color: black.withOpacity(0.1)),
                      right: BorderSide(color: black.withOpacity(0.1))
                  )),
              child:
              Center(
                child:
                ListTile(
                  trailing:Icon(Icons.arrow_forward_ios),
                  title: Text("Scan QR code",style: TextStyle(fontSize: 16,color: Colors.green,fontWeight: FontWeight.w600,),),
                  leading:  SvgPicture.asset(
                    "assets/images/scan.svg",
                    width: 50,
                    color:  Colors.green,
                  ),
                  onTap: () {

                  },
                ),
              )
          ),
        ),
      );
    }

    return Scaffold(
      appBar:
      AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Image.asset('assets/menuku.png', height: MediaQuery.of(context).size.height*0.15,),
      ),
      body:
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
         Center(
           child:  SvgPicture.asset(
             "assets/scan1.svg",
             width: 50,
             color:  Colors.green,
           ),
         ),
          Center(
            child:
              Text(
                "Scan QR code yang terdapat pada meja Anda untuk melanjutkan pesanan",textAlign: TextAlign.center,
              )
          )

        ],
      ),

      bottomSheet: getFooter(),
    );
  }
}
