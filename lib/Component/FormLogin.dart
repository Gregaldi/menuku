import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:menuku/Component/Button.dart';
import 'package:menuku/Component/LoadingView.dart';
import 'package:menuku/Component/alert.dart';
import 'package:menuku/Component/alert_style.dart';
import 'package:menuku/Component/constants.dart';
import 'package:menuku/Component/dialog_button.dart';
import 'package:menuku/Model/AccessToken.dart';
import 'package:menuku/Model/UserInformation.dart';
import 'package:menuku/Users/root_app.dart';
import 'package:menuku/utils/Session.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final FocusNode myFocusNodeEmail = FocusNode();
  final FocusNode myFocusNodePassword = FocusNode();
  TextEditingController emailUser = TextEditingController();
  TextEditingController passUser = TextEditingController();
  bool _showPass = true;

  void _toggleLogin() {
    setState(() {
      _showPass = !_showPass;
    });
  }
  void processLoginUser() async {

    Dio dio;
    Dio diologin;

    BaseOptions options = new BaseOptions(
      baseUrl: "https://api.dev.cekmenuku.com",
      receiveDataWhenStatusError: true,
        connectTimeout: 60*1000, // 60 seconds
        receiveTimeout: 60*1000,
    );

    dio = new Dio(options);

    FormData formData = new FormData.fromMap({
      "email": "apmin@menuku.com",
      "password": "qWERTY!"
    });
    Response response = await dio.post("/api/login", data: formData);
    if(response.statusCode == 200){
      AccessToken accessToken = AccessToken.fromJson(response.data);
      setAccessToken(accessToken);
      BaseOptions optionslogin = new BaseOptions(
          baseUrl: "https://api.dev.cekmenuku.com",
          receiveDataWhenStatusError: true,
          connectTimeout: 60*1000, // 60 seconds
          receiveTimeout: 60*1000,
          headers: {"Authorization": "Bearer ${accessToken.accessToken}"}
      );
      diologin = new Dio(optionslogin);
      FormData formDatalogin = new FormData.fromMap({
        "username": emailUser.text,
        "password": passUser.text,
        "type": 20
      });
      Response responselogin = await diologin.post("/api/v1/Login", data: formDatalogin,);
      if(responselogin.statusCode == 200){
        print(" user " + json.encode(responselogin.data));
        UserInformation userInformation = UserInformation.fromJson(responselogin.data);
        print(" user 1 " + userInformation.user.userClientCode);

        setUserInformation(userInformation);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Button()),
        );
        // Navigator.push(
        //   context,
        //   MaterialPageRoute(builder: (context) => RootApp()),
        // );
      }
    }
    else{
      var alertStyle = AlertStyle(
        descStyle: TextStyle(fontSize: 21),
        isCloseButton: false,
        descTextAlign: TextAlign.center,
        animationDuration:
        Duration(milliseconds: 400),
        alertBorder: RoundedRectangleBorder(
          borderRadius:
          BorderRadius.circular(0.0),
          side: BorderSide(
            color: Colors.grey,
          ),
        ),
        titleStyle: TextStyle(
          color: Colors.red,
        ),
        alertAlignment: Alignment.center,
      );
      Alert(
        context: context,
        style: alertStyle,
        type: AlertType.error,
        title: "This User does not exist",
        buttons: [
          DialogButton(
            onPressed: () {
              Navigator.of(context, rootNavigator: true).pop();
            },
            child: Text(
              "Oke",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 17),
            ),
          ),
        ],
      ).show();
    }

  }

  @override
  void initState(){
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  @override
  Widget build(BuildContext context) {

    return
      ProgressHUD(
        child: Builder(
          builder: (context) =>
             Scaffold(
               appBar: AppBar(
                 backgroundColor: Colors.green,
                 title: Text("Login"),
               ),
               body:  SingleChildScrollView(
                 child: Column(
                   mainAxisAlignment: MainAxisAlignment.start,
                   crossAxisAlignment: CrossAxisAlignment.center,
                   children: [
                     Container(
                       margin: EdgeInsets.only(top: 100),
                       child:   Center(
                         child: Image.asset("assets/intro4.png",width: MediaQuery.of(context).size.width/2,),
                       ),
                     ),
                     Container(
                         width: MediaQuery.of(context).size.width/4,
                         margin: EdgeInsets.only(top: 10),
                         child:
                         Center(
                           child:  Divider(thickness: 10,),
                         )
                     ),
                     Container(
                       child: Form(
                           key: _formKey,
                           child: Column(
                             children: [
                               Container(
                                   margin: EdgeInsets.only(
                                       top: 22, left: 15),
                                   child: Column(
                                     crossAxisAlignment:
                                     CrossAxisAlignment.start,
                                     children: [
                                       Text(
                                         'Username',
                                         style: TextStyle(
                                             fontSize: 13,
                                             fontFamily:
                                             'Inter'),
                                       ),
                                       Container(
                                         margin: EdgeInsets.only(
                                             right: 17),
                                         child: TextFormField(
                                           validator: (value){
                                             if(value!.isEmpty)
                                               return 'Username Harus diisi';
                                             // if(!RegExp(r'^.+@[a-zA-Z]+\.{1}[a-zA-Z]+(\.{0,1}[a-zA-Z]+)$').hasMatch(value))
                                             //   return 'Email Tidak Sesuai Format';
                                           },
                                           focusNode:
                                           myFocusNodeEmail,
                                           controller: emailUser,
                                           decoration:
                                           InputDecoration(
                                             hintText:
                                             "Masukan Username anda",
                                             hintStyle: TextStyle(
                                                 fontSize: 15,
                                                 fontFamily:
                                                 'Roboto'),
                                             focusedBorder:
                                             UnderlineInputBorder(
                                               borderSide:
                                               BorderSide(
                                                 color: Color
                                                     .fromRGBO(
                                                     127,
                                                     40,
                                                     39,
                                                     1),
                                               ),
                                             ),
                                           ),
                                         ),
                                       )
                                     ],
                                   )
                               ),
                               Container(
                                   margin: EdgeInsets.only(
                                       top: 22, left: 15,bottom: 10),
                                   child: Column(
                                     crossAxisAlignment:
                                     CrossAxisAlignment.start,
                                     children: [
                                       Text(
                                         'Kata sandi',
                                         style: TextStyle(
                                             fontSize: 13,
                                             fontFamily:
                                             'Roboto'),
                                       ),
                                       Container(
                                         margin: EdgeInsets.only(
                                             right: 17),
                                         child: TextFormField(
                                           validator: (value){
                                             if(value!.isEmpty)
                                               return 'Password harus diisi';
                                             // if(!RegExp(r'^.{8,}').hasMatch(value))
                                             // return 'Password Harus Minimal 8 Karakter';
                                           },
                                           focusNode:
                                           myFocusNodePassword,
                                           controller: passUser,
                                           obscureText: _showPass,
                                           decoration:
                                           InputDecoration(
                                             hintText:
                                             "Masukan kata sandi anda",
                                             hintStyle: TextStyle(
                                                 fontSize: 15,
                                                 fontFamily:
                                                 'Roboto'),
                                             focusedBorder:
                                             UnderlineInputBorder(
                                               borderSide:
                                               BorderSide(
                                                 color: Color
                                                     .fromRGBO(
                                                     127,
                                                     40,
                                                     39,
                                                     1),
                                               ),
                                             ),
                                             suffixIcon:
                                             GestureDetector(
                                               onTap: _toggleLogin,
                                               child: Icon(
                                                 _showPass
                                                     ? Icons.remove_red_eye_sharp
                                                     : Icons.remove_red_eye,
                                                 size: 15.0,
                                                 color:
                                                 Colors.black,
                                               ),
                                             ),
                                           ),
                                         ),
                                       )
                                     ],
                                   )),
                               Container(
                                   margin: EdgeInsets.only(top: 5),
                                   width: MediaQuery.of(context).size.width/2,
                                   child:
                                   RaisedButton(
                                     shape: const RoundedRectangleBorder(
                                       borderRadius: BorderRadius.all(Radius.circular(10)),
                                     ),
                                     color: Colors.green,
                                     autofocus: true,
                                     onPressed: () async {

                                       if(_formKey.currentState!.validate()){
                                         final progress = ProgressHUD.of(context);
                                         progress?.show();
                                         processLoginUser();
                                         Future.delayed(Duration(seconds: 1), () {
                                           progress?.dismiss();
                                         });
                                       }
                                     },
                                     child: Text('Login',style: TextStyle(
                                         fontSize: 14,
                                         fontFamily: 'Inter-Regular',
                                         fontWeight: FontWeight.normal,
                                         color: Colors.white
                                     )),

                                   )
                               ),


                             ],
                           )
                       ),
                     )
                   ],
                 ),
               ),
             )
        ),
      );
  }
}
