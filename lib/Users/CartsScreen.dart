import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:menuku/Model/GetCartModel.dart';
import 'package:menuku/Model/UserInformation.dart';
import 'package:menuku/utils/Session.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  late String token;
  UserInformation? userInformation;
  List<GetCartModel>? getCartModel;

  _gettoken() async {
    var data = await getAccessToken();
    setState(() {
      token = data!.accessToken;
      print(" token b2b " + token);
    });
  }
  _getUserInformation() async {
    var data = await getUserInformation();
    setState(() {
      userInformation = data;
      print(" user " + json.encode(data));

    });
  }
  Future getcart() async {
    Map body = {
      "user_client_code": userInformation!.user.userClientCode
    };
    List<GetCartModel> country = <GetCartModel>[];
    var response = await http.post(
        Uri.parse("https://api.menuku.subredtis.com/api/v1/getCart"),
        headers: {
          "Authorization": "Bearer "+token,
          "content-type": "application/json"
        },
        body:json.encode(body)
    );
    Iterable list = json.decode(response.body);
    var temp = <GetCartModel>[];
    temp = list.map((model) => GetCartModel.fromJson(model)).toList();
    country.addAll(temp);
    return country;
  }


  @override
  void initState() {
    super.initState();
    _gettoken();
    _getUserInformation();
    getcart();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar:
      AppBar(
        backgroundColor: Colors.green,
        title: Text(
            'Pesanan Anda',
            style: TextStyle(
                fontSize: 17,
                fontFamily: 'Inter-Regular',
                fontWeight: FontWeight.normal,
                color: Colors.white
            )
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Card(
              margin: EdgeInsets.only(top: 10,),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                    Container(
                      child:Text(
                        "Item Pesanan",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.normal, color: Colors.black),
                      ),
                      margin: EdgeInsets.only(top: 10,left: 10),
                    ),
                      IconButton(
                        icon: Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                              color: Colors.white, shape: BoxShape.circle),
                          child: Center(
                            child: Icon(
                              Icons.create_sharp,
                              size: 18,
                            ),
                          ),
                        ),
                        onPressed: () {
                        },
                      )

                    ],
                  ),
                  FutureBuilder(
                      future: getcart(),
                      builder: (context, AsyncSnapshot snapshot) {
                        switch (snapshot.connectionState) {
                          case ConnectionState.none:
                            return Text('Press button to start.');
                          case ConnectionState.active:
                          case ConnectionState.waiting:
                            return
                              Center(
                                  child: CupertinoActivityIndicator()
                              )
                            ;
                          case ConnectionState.done:
                            if(snapshot.hasData){
                              getCartModel = snapshot.data as List<GetCartModel>? ;
                              print(" data " +snapshot.data.toString());
                              Codec<String, String> stringToBase64 = utf8.fuse(base64);
                              final formatCurrency = new NumberFormat.currency(
                                  locale: 'ID', symbol: "Rp.", decimalDigits: 0);

                              double sum = getCartModel!.map((expense) => expense.price).fold(0, (prev, amount) => prev + int.parse(amount));
                              return
                                ListView.builder(
                                  itemCount: getCartModel!.length,
                                  shrinkWrap: true,
                                  itemBuilder: (context, index){
                                    String encoded = stringToBase64.decode(getCartModel![index].pictures);
                                    return
                                      Row(
                                      children: [
                                        Container(
                                          height: 120,
                                          child: Padding(
                                            padding: EdgeInsets.only(
                                                left: 20, top: 10, bottom: 10),
                                            child:
                                            Stack(
                                              children: [
                                                Image(
                                                  image: NetworkImage(
                                                    "https://apmin.dev.cekmenuku.com/$encoded",
                                                  ),
                                                  fit: BoxFit.cover,
                                                  height: 200,
                                                  width: 130,
                                                  errorBuilder: (context, url, error) => Image.network('https://career.astra.co.id/static/media/image_not_available1.94c0c57d.png'),
                                                ),
                                                Positioned(
                                                    left: 110.0,
                                                    // top: -40.0,
                                                    child:
                                                    Text("${getCartModel![index].qty} x ",style: TextStyle(
                                                        fontSize: 14, fontWeight: FontWeight.bold, color: Colors.orange),)
                                                ),

                                              ],
                                            )

                                          ),
                                        ),
                                        Container(
                                          height: 110,
                                          child: Padding(
                                              padding: EdgeInsets.only(
                                                  left: 20, top: 10, bottom: 10),
                                              child:
                                              Column(
                                                children: [
                                                  Container(
                                                    child: Text("${getCartModel![index].name} ",style: TextStyle(
                                                        fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),) ,
                                                  ),
                                                  Container(
                                                    child:
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: [
                                                            Container(
                                                              child: Text("${formatCurrency.format(int.parse(getCartModel![index].price))} ",style: TextStyle(
                                                                  fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),),
                                                            ),

                                                            Container(
                                                              child:
                                                              Text("TAMBAH item",style: TextStyle(
                                                                  fontSize: 14, fontWeight: FontWeight.bold, color: Colors.green),),
                                                              margin: EdgeInsets.only(left: 20),
                                                            ),
                                                          ],
                                                        ),
                                                    margin: EdgeInsets.only(top: 40),
                                                  ),
                                                ],
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                              )
                                          ),
                                        ),

                                      ],
                                    );
                                  },
                                );
                            }
                            return
                                Container(
                                  child: Center(
                                    child: Text(
                                      "Tidak Ada Pesanan"
                                    ),
                                  ),
                                );
                        }
                      }
                  ),
                ],
              ),
            ),
            Card(
              margin: EdgeInsets.only(top: 10,),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child:Text(
                          "Rencana Pembayaran",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.normal, color: Colors.black),
                        ),
                        margin: EdgeInsets.only(top: 10,left: 10),
                      ),
                      IconButton(
                        icon: Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                              color: Colors.white, shape: BoxShape.circle),
                          child: Center(
                            child: Icon(
                              Icons.create_sharp,
                              size: 18,
                            ),
                          ),
                        ),
                        onPressed: () {
                        },
                      )

                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Row(

                        children: [
                          IconButton(
                            icon: Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                  color: Colors.white, shape: BoxShape.circle),
                              child: Center(
                                child: Icon(
                                  Icons.money,
                                  size: 30,
                                ),
                              ),
                            ),
                            onPressed: () {
                            },
                          ),
                          Row(
                            children: [
                              Container(
                                child:Text(
                                  "Tunai",
                                  style: TextStyle(
                                      fontSize: 14, fontWeight: FontWeight.normal, color: Colors.black),
                                ),
                                margin: EdgeInsets.only(top: 10,left: 10),
                              ),
                              Container(
                                child:Text(
                                  "(bayar di kasir)",
                                  style: TextStyle(
                                      fontSize: 14, fontWeight: FontWeight.normal, color: Colors.grey),
                                ),
                                margin: EdgeInsets.only(top: 10,left: 10),
                              ),
                            ],
                          ),
                        ],
                        mainAxisAlignment: MainAxisAlignment.start,
                      ),
                      Container(
                        child:Text(
                          "TAMBAH PROMO",
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.normal, color: Colors.green),
                        ),
                        margin: EdgeInsets.only(top: 10,left: 10),
                      ),
                    ],
                  )

                ],
              ),
            ),
            Card(
              margin: EdgeInsets.only(top: 10,),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child:Text(
                          "Detil Pembayaran",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.normal, color: Colors.black),
                        ),
                        margin: EdgeInsets.only(top: 10,left: 10),
                      ),
                      IconButton(
                        icon: Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                              color: Colors.white, shape: BoxShape.circle),
                          child: Center(
                            child: Icon(
                              Icons.create_sharp,
                              size: 18,
                            ),
                          ),
                        ),
                        onPressed: () {
                        },
                      )

                    ],
                  ),
                  FutureBuilder(
                      future: getcart(),
                      builder: (context, AsyncSnapshot snapshot) {
                        switch (snapshot.connectionState) {
                          case ConnectionState.none:
                            return Text('Press button to start.');
                          case ConnectionState.active:
                          case ConnectionState.waiting:
                            return
                              Center(
                                  child: CupertinoActivityIndicator()
                              )
                            ;
                          case ConnectionState.done:
                            if(snapshot.hasData){
                              getCartModel = snapshot.data as List<GetCartModel>? ;
                              print(" data " +snapshot.data.toString());
                              Codec<String, String> stringToBase64 = utf8.fuse(base64);
                              final formatCurrency = new NumberFormat.currency(
                                  locale: 'ID', symbol: "Rp.", decimalDigits: 0);

                              double sum = getCartModel!.map((expense) => expense.price).fold(0, (prev, amount) => prev + int.parse(amount));
                              return
                                ListView.builder(
                                  itemCount: 1,
                                  shrinkWrap: true,
                                  itemBuilder: (context, index){
                                    double sum = getCartModel!.map((expense) => expense.price).fold(0, (prev, amount) => prev + int.parse(amount));

                                    return
                                      Column(
                                        children: [
                                          Container(
                                            child: Padding(
                                                padding: EdgeInsets.only(
                                                    left: 20,bottom: 10,top: 10),
                                                child:
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      child: Text("Subtotal",style: TextStyle(
                                                          fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),),
                                                    ),
                                                    Container(
                                                      child: Text("${formatCurrency.format(sum)} ",style: TextStyle(
                                                          fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),) ,
                                                      margin: EdgeInsets.only(right: 15),
                                                    ),
                                                  ],
                                                )


                                            ),
                                          ),

                                        ],
                                      );
                                  },
                                );
                            }
                            return
                              Container(
                                child: Center(
                                  child: Text(
                                      "Tidak Ada Pesanan"
                                  ),
                                ),
                              );
                        }
                      }
                  ),

                ],
              ),
            ),

          ],
        ),
      ),
      bottomSheet:  Card(
        margin: EdgeInsets.only(top: 10,),
        child:
        FutureBuilder(
            future: getcart(),
            builder: (context, AsyncSnapshot snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                  return Text('Press button to start.');
                case ConnectionState.active:
                case ConnectionState.waiting:
                  return
                    Center(
                        child: CupertinoActivityIndicator()
                    )
                  ;
                case ConnectionState.done:
                  if(snapshot.hasData){
                    getCartModel = snapshot.data as List<GetCartModel>? ;
                    print(" data " +snapshot.data.toString());
                    Codec<String, String> stringToBase64 = utf8.fuse(base64);
                    final formatCurrency = new NumberFormat.currency(
                        locale: 'ID', symbol: "Rp.", decimalDigits: 0);
                    double sum = getCartModel!.map((expense) => expense.price).fold(0, (prev, amount) => prev + int.parse(amount));
                    return
                      ListView.builder(
                        itemCount: 1,
                        shrinkWrap: true,
                        itemBuilder: (context, index){
                          double sum = getCartModel!.map((expense) => expense.price).fold(0, (prev, amount) => prev + int.parse(amount));

                          return
                            Column(
                              children: [
                                Container(
                                  child: Padding(
                                      padding: EdgeInsets.only(
                                          left: 20,bottom: 10,top: 10),
                                      child:
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            child: Text("Total",style: TextStyle(
                                                fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),),
                                          ),
                                          Container(
                                            child: Text("${formatCurrency.format(sum)} ",style: TextStyle(
                                                fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),) ,
                                            margin: EdgeInsets.only(right: 15),
                                          ),
                                        ],
                                      )


                                  ),
                                ),
                                Container(
                                    margin: EdgeInsets.only(top: 2),
                                    height:60 ,
                                    width: MediaQuery.of(context).size.width/1.1,
                                    child:
                                    RaisedButton(
                                      shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                      ),
                                      color: Colors.green,
                                      autofocus: true,
                                      onPressed: () async {

                                      },
                                      child: Text('BAYAR PESANAN',style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: 'Inter-Regular',
                                          fontWeight: FontWeight.normal,
                                          color: Colors.white
                                      )),

                                    )
                                ),

                              ],
                            );
                        },
                      );
                  }
                  return
                    Container(
                      child: Center(
                        child: Text(
                            "Tidak Ada Pesanan"
                        ),
                      ),
                    );
              }
            }
        ),

      ),
    );
  }
}
