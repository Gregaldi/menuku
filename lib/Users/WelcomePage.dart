import 'package:flutter/material.dart';
import 'package:menuku/Component/FormLogin.dart';
import 'package:menuku/Component/FormRegister.dart';
import 'package:menuku/Component/LoadingView.dart';
import 'package:menuku/utils/fontstyle.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return
      ProgressHUD(
        child:
        Scaffold(
            body:
            SafeArea(
                child:
                SingleChildScrollView(
                  child:
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 100),
                        child:   Center(
                          child: Image.asset("assets/intro4.png",width: MediaQuery.of(context).size.width/2,),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text("Welcome",style: Inter24Bold,),
                        ),
                      ),
                      Container(
                        child: Align(
                          alignment: Alignment.center,
                          child: Text("Before enjoying MENUKU services",style: InterRegular14,),
                        ),
                      ),
                      Container(
                        child: Align(
                          alignment: Alignment.center,
                          child: Text("Please register first",style: InterRegular14,),
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(top: 30),
                          width: MediaQuery.of(context).size.width/2,
                          child:
                          RaisedButton(
                            shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                            ),
                            color: Colors.green,
                            autofocus: true,
                            onPressed: () async{
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => RegisterForm()),
                              );
                              // showModalBottomSheet(
                              //   context: context,
                              //   isScrollControlled: true,
                              //   shape: RoundedRectangleBorder(
                              //     borderRadius: BorderRadius.only(
                              //       topLeft: const Radius.circular(40.0),
                              //       topRight: const Radius.circular(40.0),),
                              //   ),
                              //   backgroundColor: Colors.transparent,
                              //   builder: (context) => Container(
                              //       height: MediaQuery.of(context).size.height * 0.8,
                              //       decoration: new BoxDecoration(
                              //         color: Colors.white,
                              //         borderRadius: new BorderRadius.only(
                              //           topLeft: const Radius.circular(25.0),
                              //           topRight: const Radius.circular(25.0),
                              //         ),
                              //       ),
                              //       child:  RegisterForm()
                              //   ),
                              // );
                            },
                            child: Text('Create Account',style: TextStyle(
                                fontSize: 14,
                                fontFamily: 'Inter-Regular',
                                fontWeight: FontWeight.normal,
                                color: Colors.white
                            )),

                          )
                      ),
                      Container(
                        child: RaisedButton(
                          onPressed: () async{
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => LoginForm()),
                            );
                            // showModalBottomSheet(
                            //   context: context,
                            //   isScrollControlled: true,
                            //   shape: RoundedRectangleBorder(
                            //     borderRadius: BorderRadius.only(
                            //       topLeft: const Radius.circular(40.0),
                            //       topRight: const Radius.circular(40.0),),
                            //   ),
                            //   backgroundColor: Colors.transparent,
                            //   builder: (context) => Container(
                            //       height: MediaQuery.of(context).size.height * 0.8,
                            //       decoration: new BoxDecoration(
                            //         color: Colors.white,
                            //         borderRadius: new BorderRadius.only(
                            //           topLeft: const Radius.circular(25.0),
                            //           topRight: const Radius.circular(25.0),
                            //         ),
                            //       ),
                            //       child:  LoginForm()
                            //   ),
                            // );
                          },
                          child: Text(
                            "Login",style: TextStyle(color: Colors.green,fontFamily: 'Inter',fontSize: 14),
                          ),
                          shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(Radius.circular(10))
                          ),
                          color: Color.fromRGBO(209, 250, 229, 100),
                        ),
                        width: MediaQuery.of(context).size.width/2,
                      ),
                      Container(
                          margin: EdgeInsets.only(left: 50,right: 50,top: 10),
                          child: Align(
                            child: Text("By logging in or registering, you have agreed to the Terms and"
                              ,style: TextStyle(fontSize: 10,fontFamily: 'Inter-Bold',color: Colors.black),),
                          )
                      ),
                      Container(
                          margin: EdgeInsets.only(left: 50,right: 50),
                          child: Align(
                            child: Text("Conditions and Privacy Policy."
                              ,style: TextStyle(fontSize: 10,fontFamily: 'Inter-Bold',color: Colors.black),),
                          )
                      )
                    ],
                  ),
                ))
        )
      );
  }
}
