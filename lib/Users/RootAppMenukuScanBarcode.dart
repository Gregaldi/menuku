import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:menuku/Component/Dashboard.dart';
import 'package:menuku/Users/HomeScreen.dart';
import 'package:menuku/Users/Pesanan.dart';
import 'package:menuku/Users/ProfileAccount.dart';
import 'package:menuku/utils/colors.dart';


class RootApp1 extends StatefulWidget {
  @override
  _RootApp1State createState() => _RootApp1State();
}

class _RootApp1State extends State<RootApp1> {
  int pageIndex = 0;



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: getBody(),
      bottomNavigationBar: getFooter(),
    );
  }

  Widget getBody() {
    List<Widget> pages = [
      Dashboard(),
      Pesanan(),
      Center(
        child: Text(
          "Promo",
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: black),
        ),
      ),
      AccountProfile()
    ];
    return IndexedStack(
      index: pageIndex,
      children: pages,
    );
  }

  Widget getFooter() {
    List bottomItems = [
      "assets/images/home_icon.svg",
      "assets/images/order_icon.svg",
      "assets/images/promo.svg",
      "assets/images/account_icon.svg"
    ];
    List textItems = ["Home", "Pesanan", "Promo", "Account"];
    return
      Container(
        width: double.infinity,
        height: 60,
        decoration: BoxDecoration(
            color: white,
            border: Border(
                top: BorderSide(width: 2, color: black.withOpacity(0.06)))),
        child:
        Padding(
          padding:
          const EdgeInsets.only(left: 20, right: 20, top: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: List.generate(textItems.length, (index) {
              return InkWell(
                  onTap: () {
                    selectedTab(index);

                  },
                  child: Column(
                    children: [
                      SvgPicture.asset(
                        bottomItems[index],
                        width: 22,
                        color: pageIndex == index ? black : Colors.grey,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        textItems[index],
                        style: TextStyle(
                            fontSize: 10,
                            color: pageIndex == index
                                ? black
                                : black.withOpacity(0.5)),
                      )
                    ],
                  ));
            }),
          ),
        ),
      );
  }

  selectedTab(index) {
    setState(() {
      pageIndex = index;
    });
  }
}
