import 'package:flutter/material.dart';

class Pesanan extends StatefulWidget {
  const Pesanan({Key? key}) : super(key: key);

  @override
  _PesananState createState() => _PesananState();
}

class _PesananState extends State<Pesanan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
      AppBar(
        backgroundColor: Colors.green,
        automaticallyImplyLeading: false,
        title: Text(
            'Pesanan Anda',
            style: TextStyle(
                fontSize: 17,
                fontFamily: 'Inter-Regular',
                fontWeight: FontWeight.normal,
                color: Colors.white
            )
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Card(
              margin: EdgeInsets.only(left: 10,right: 10,top: 10),
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10,bottom: 10,top: 10),
                        child: Text(
                          "Meja A14"
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 10,bottom: 10,top: 10),
                        child: Text(
                            "31 Juli 2022"
                        ),
                      )
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  ),
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10,bottom: 5,top: 10),
                        child: Text(
                            "Rp 49,500"
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 10,bottom: 5,top: 10),
                        child: Text(
                            "(1 item)"
                        ),
                      )
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  ),
                  Divider(
                    color: Colors.black.withOpacity(0.3),
                  ),
                  Container(
                    color: Colors.white,
                    child:
                    ListTile(
                      trailing: GestureDetector(
                        child:
                        Icon(
                          Icons.arrow_forward_ios,
                          size: 18,
                        ),
                      ),
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.assignment,
                            size: 18,
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            child:   Text("Bayar pesanan", style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'Inter-Regular',
                                fontWeight: FontWeight.normal,
                                color: Colors.black
                            )),
                          ),
                        ],
                      ),
                      onTap: () {},
                    ),
                  ),


                ],
              ),
            ),
            Card(
              margin: EdgeInsets.only(left: 10,right: 10,top: 10),
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10,bottom: 10,top: 10),
                        child: Text(
                            "Meja A14"
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 10,bottom: 10,top: 10),
                        child: Text(
                            "31 Juli 2022"
                        ),
                      )
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  ),
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10,bottom: 5,top: 10),
                        child: Text(
                            "Rp 49,500"
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 10,bottom: 5,top: 10),
                        child: Text(
                            "(1 item)"
                        ),
                      )
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  ),
                  Divider(
                    color: Colors.black.withOpacity(0.3),
                  ),
                  Container(
                    color: Colors.white,
                    child:
                    ListTile(
                      trailing: GestureDetector(
                        child:
                        Icon(
                          Icons.arrow_forward_ios,
                          size: 18,
                        ),
                      ),
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.verified,
                            size: 18,
                              color: Colors.green
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            child:   Text("Pembayaran berhasil", style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'Inter-Regular',
                                fontWeight: FontWeight.normal,
                                color: Colors.green
                            )),
                          ),
                        ],
                      ),
                      onTap: () {},
                    ),
                  ),


                ],
              ),
            )

          ],
        ),
      ),
    );
  }
}
