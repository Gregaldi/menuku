import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:menuku/Component/alert.dart';
import 'package:menuku/Component/alert_style.dart';
import 'package:menuku/Component/constants.dart';
import 'package:menuku/Component/dialog_button.dart';
import 'package:menuku/Model/ErrorResponse.dart';
import 'package:menuku/Model/ResponsesSucessCart.dart';
import 'package:menuku/Model/UserInformation.dart';
import 'package:menuku/Users/MenuScreen.dart';
import 'package:menuku/Users/WelcomePage.dart';
import 'package:menuku/utils/Session.dart';
import 'package:menuku/utils/colors.dart';
import 'package:menuku/utils/styles.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddToBasket extends StatefulWidget {
  final String  body, id;
  final String nama, menus_resto_code,price,price_after_disc,desc,disc_percent,disc_amount,pictures;
  const AddToBasket({required this.nama,required this.menus_resto_code,
    required this.price,required this.price_after_disc,
    required this.desc,required this.disc_percent,
    required this.disc_amount,required this.pictures,
    required this.body,required this.id});


  @override
  _AddToBasketState createState() => _AddToBasketState();
}

class _AddToBasketState extends State<AddToBasket> {
  late String token;
  TextEditingController qty = TextEditingController();
  ErrorResponse? errorResponse;
  ResponseSuccesCart? responseSuccesCart;
  UserInformation? userInformation;
  final formatCurrency = new NumberFormat.currency(
      locale: 'ID', symbol: "Rp.", decimalDigits: 0);

  _getUserInformation() async {
    var data = await getUserInformation();
    setState(() {
      userInformation = data;
      print(" user " + json.encode(data));

    });
  }

  _gettoken() async {
    var data = await getAccessToken();
    setState(() {
      token = data!.accessToken;
      print(" token b2b " + token);
    });
  }


  @override
  void initState() {
    super.initState();
    _gettoken();
    _getUserInformation();
    qty.text = "1";
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: getBody(),
      bottomSheet: getFooter(),
    );
  }

  Widget getFooter() {
    var size = MediaQuery.of(context).size;
    int harga = int.parse(qty.text) * int.parse(widget.price);
    return Container(
      height: 80,
      width: size.width,
      decoration: BoxDecoration(
          color: white,
          border: Border(top: BorderSide(color: black.withOpacity(0.1)))),
      child: Padding(
        padding: const EdgeInsets.only(top: 15),
        child: Column(
          children: [
            int.parse(qty.text) == 0?
            Container(
                margin: EdgeInsets.only(top: 2),
                height:60 ,
                width: MediaQuery.of(context).size.width/1.1,
                child:
                RaisedButton(
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  color: Colors.red,
                  autofocus: true,
                  onPressed: () async {
                    Dio dio;
                    BaseOptions options = new BaseOptions(
                      baseUrl: "https://api.dev.cekmenuku.com",
                      receiveDataWhenStatusError: true,
                      headers: {
                        "Authorization": "Bearer "+token,
                      },
                      connectTimeout: 60*1000, // 60 seconds
                      receiveTimeout: 60*1000,
                    );

                    dio = new Dio(options);

                    FormData formData = new FormData.fromMap({
                      "user_client_code": userInformation!.user.userClientCode,
                      "menu_resto_code": widget.menus_resto_code,
                    });
                    try {
                      Response response = await dio.post("/api/v1/deleteCart", data: formData);

                      if(response.statusCode == 200){
                        if(response.data['status']== 200){
                          responseSuccesCart = ResponseSuccesCart.fromJson(response.data);
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => MenuScreen(body: widget.body, id: widget.id)),
                          );

                        }else{
                          errorResponse = ErrorResponse.fromJson(response.data);
                          print(response.data['status'].toString());
                        }
                      }

                    }on DioError  catch (ex) {
                      if(ex.type == DioErrorType.connectTimeout){
                        var alertStyle = AlertStyle(
                          descStyle: TextStyle(fontSize: 21),
                          isCloseButton: false,
                          isOverlayTapDismiss: false,
                          descTextAlign: TextAlign.center,
                          animationDuration:
                          Duration(milliseconds: 400),
                          alertBorder: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.circular(0.0),
                            side: BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                          titleStyle: TextStyle(
                            color: Colors.red,
                          ),
                          alertAlignment: Alignment.center,
                        );
                        Alert(
                          context: context,
                          style: alertStyle,
                          type: AlertType.error,
                          title: "Connection Timeout",
                          buttons: [
                            DialogButton(
                              onPressed: () {
                                Navigator.of(context, rootNavigator: true).pop();

                              },
                              child: Text(
                                "Oke",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17),
                              ),
                            ),
                          ],).show();
                        throw Exception("Connection Timeout Exception");

                      }

                    }

                  },
                  child: Text("Remove",style: TextStyle(
                      fontSize: 14,
                      fontFamily: 'Inter-Regular',
                      fontWeight: FontWeight.normal,
                      color: Colors.white
                  )),

                )
            )
                :
            Container(
                margin: EdgeInsets.only(top: 2),
                height:60 ,
                width: MediaQuery.of(context).size.width/1.1,
                child:
                RaisedButton(
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  color: Colors.green,
                  autofocus: true,
                  onPressed: () async {
                    Dio dio;
                    BaseOptions options = new BaseOptions(
                      baseUrl: "https://api.dev.cekmenuku.com",
                      receiveDataWhenStatusError: true,
                      headers: {
                        "Authorization": "Bearer "+token,
                      },
                      connectTimeout: 60*1000, // 60 seconds
                      receiveTimeout: 60*1000,
                    );

                    dio = new Dio(options);

                    FormData formData = new FormData.fromMap({
                        "user_client_code": userInformation!.user.userClientCode,
                        "menu_resto_code": widget.menus_resto_code,
                        "qty": int.parse(qty.text)

                    });
                    try {
                      Response response = await dio.post("/api/v1/storeCart", data: formData);

                      if(response.statusCode == 200){
                        if(response.data['status']== 200){
                          responseSuccesCart = ResponseSuccesCart.fromJson(response.data);
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => MenuScreen(body: widget.body, id: widget.id)),
                          );


                        }else{
                          errorResponse = ErrorResponse.fromJson(response.data);
                          print(response.data['status'].toString());
                        }
                      }

                    }on DioError  catch (ex) {
                      if(ex.type == DioErrorType.connectTimeout){
                        var alertStyle = AlertStyle(
                          descStyle: TextStyle(fontSize: 21),
                          isCloseButton: false,
                          isOverlayTapDismiss: false,
                          descTextAlign: TextAlign.center,
                          animationDuration:
                          Duration(milliseconds: 400),
                          alertBorder: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.circular(0.0),
                            side: BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                          titleStyle: TextStyle(
                            color: Colors.red,
                          ),
                          alertAlignment: Alignment.center,
                        );
                        Alert(
                          context: context,
                          style: alertStyle,
                          type: AlertType.error,
                          title: "Connection Timeout",
                          buttons: [
                            DialogButton(
                              onPressed: () {
                                Navigator.of(context, rootNavigator: true).pop();

                              },
                              child: Text(
                                "Oke",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17),
                              ),
                            ),
                          ],).show();
                        throw Exception("Connection Timeout Exception");

                      }
                    }

                  },
                  child: Text('Add to Basket - ${formatCurrency.format(harga)}',style: TextStyle(
                      fontSize: 14,
                      fontFamily: 'Inter-Regular',
                      fontWeight: FontWeight.normal,
                      color: Colors.white
                  )),

                )
            ),

          ],
        ),
      ),
    );
  }

  Widget getBody() {
    int _itemCount = 0;
    var size = MediaQuery.of(context).size;
    Codec<String, String> stringToBase64 = utf8.fuse(base64);
    String encoded = stringToBase64.decode(widget.pictures);
    return
      SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(bottom: 60),
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    width: size.width,
                    height: 150,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: NetworkImage("https://apmin.dev.cekmenuku.com/$encoded"), fit: BoxFit.cover)),
                  ),
                  SafeArea(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                          icon: Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                color: white, shape: BoxShape.circle),
                            child: Center(
                              child: Icon(
                                Icons.arrow_back,
                                size: 18,
                              ),
                            ),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        IconButton(
                          icon: Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                color: white, shape: BoxShape.circle),
                            child: Center(
                              child: Icon(
                                Icons.favorite_border,
                                size: 18,
                              ),
                            ),
                          ),
                          onPressed: () {},
                        )
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          widget.nama,
                          style: TextStyle(fontSize: 21, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          formatCurrency.format(int.parse(widget.price)),
                          style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      children: [
                        Container(
                          width: size.width - 30,
                          child: Text(
                            widget.desc,
                            style: TextStyle(fontSize: 14, height: 1.3),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Divider(
                      color: black.withOpacity(0.3),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Note Resto (optional)",
                      style: customContent,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      decoration:
                      InputDecoration(
                        hintText:
                        "Add your request ( subject to restaurant discretion )",
                        hintStyle: TextStyle(
                            fontSize: 15,),
                        focusedBorder:
                        UnderlineInputBorder(
                          borderSide:
                          BorderSide(
                            color: Color
                                .fromRGBO(
                                127,
                                40,
                                39,
                                1),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
          padding: const EdgeInsets.only(left: 15, right: 15,top: 50),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.only(right: 10,bottom: 10),
                    child:
                    InkWell(
                      child: Icon(
                        Icons.remove,
                        size: 22.0,
                      ),
                      onTap: () {
                        int currentValue = int.parse(qty.text);
                        setState(() {
                          currentValue--;
                          qty.text =
                              (currentValue > 1 ? currentValue : 0)
                                  .toString(); // decrementing value
                        });
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 5),
                    width: MediaQuery.of(context).size.width / 5.5,
                    height: 70,
                    child: TextFormField(
                      textAlign: TextAlign.center,
                      controller: qty,
                      readOnly: true,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 10,bottom: 10),
                    child:
                    InkWell(
                      child: Icon(
                        Icons.add,
                        color: Colors.red,
                        size: 22.0,
                      ),
                      onTap: () {
                        int currentValue = int.parse(qty.text);
                        setState(() {
                          currentValue++;
                          qty.text =
                              (currentValue < 9 ? currentValue : 9)
                                  .toString(); // incrementing value
                        });
                      },
                    ),
                  ),


                ],
              )
            ],
          ),
        ),



            ],
          ),
        ),
      );
  }
}