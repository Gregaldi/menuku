import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:menuku/Model/UserInformation.dart';
import 'package:menuku/Users/WelcomePage.dart';
import 'package:menuku/utils/Session.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountProfile extends StatefulWidget {
  const AccountProfile({Key? key}) : super(key: key);

  @override
  _AccountProfileState createState() => _AccountProfileState();
}

class _AccountProfileState extends State<AccountProfile> {

  UserInformation? userInformation;
  _logout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.clear();
    userInformation == null;
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => WelcomePage()));
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => WelcomePage()),
    // );
  }
  _getUserInformation() async {
    var data = await getUserInformation();
    setState(() {
      userInformation = data;
    });
  }

  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
      AppBar(
        backgroundColor: Colors.green,
        automaticallyImplyLeading: false,
        title: Text(
          'Account Profile',
            style: TextStyle(
            fontSize: 17,
            fontFamily: 'Inter-Regular',
            fontWeight: FontWeight.normal,
            color: Colors.white
        )
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Card(
              margin: EdgeInsets.only(left: 12,right: 12,top: 10),
              child:
              Container(
                color: Colors.white,
                child:
                ListTile(
                  trailing: GestureDetector(
                    child:
                      Container(
                      margin: EdgeInsets.only(top: 15,bottom: 10),
                      child:
                      Text("EDIT", style: TextStyle(
                          fontSize: 17,
                          fontFamily: 'Inter-Regular',
                          fontWeight: FontWeight.bold,
                          color: Colors.green
                      )
                    ),

                 ),
                  ),
                  title: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10,bottom: 5),
                        child:   Text(userInformation == null?"":userInformation!.user.username, style: TextStyle(
                            fontSize: 20,
                            fontFamily: 'Inter-Regular',
                            fontWeight: FontWeight.normal,
                            color: Colors.black
                        )),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5,bottom: 10),
                        child:   Text(userInformation == null?"":userInformation!.user.email, style: TextStyle(
                            fontSize: 17,
                            fontFamily: 'Inter-Regular',
                            fontWeight: FontWeight.normal,
                            color: Colors.black
                        )),
                      ),


                    ],
                  ),
                  onTap: () {},
                ),
              ),
            ),

            Card(
              margin: EdgeInsets.only(left: 12,right: 12,top: 10),
              child:
              Container(
                color: Colors.white,
                child: ListTile(
                  trailing: Icon(
                    Icons.navigate_next,
                    color: Colors.grey,
                    size: 20,
                  ),
                  title: Text("Syarat dan ketentuan",),
                  onTap: () {},
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.only(left: 12,right: 12,top: 10),
              child:
              Container(
                color: Colors.white,
                child: ListTile(
                  trailing: Icon(
                    Icons.navigate_next,
                    color: Colors.grey,
                    size: 20,
                  ),
                  title: Text("Kebijakan privasi",),
                  onTap: () {},
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.only(left: 12,right: 12,top: 10),
              child:
              Container(
                color: Colors.white,
                child: ListTile(
                  trailing: Icon(
                    Icons.navigate_next,
                    color: Colors.grey,
                    size: 20,
                  ),
                  title: Text("Tentang Menuku",),
                  onTap: () {},
                ),
              ),
            ),
            GestureDetector(
              onTap: (){
                _logout();
              },
              child:
              Card(
                margin: EdgeInsets.only(left: 12,right: 12,top: 10),
                child:
                Container(
                  color: Colors.white,
                  child: ListTile(
                    trailing: Icon(
                      Icons.navigate_next,
                      color: Colors.grey,
                      size: 20,
                    ),
                    title: Text("Log Out",),
                  ),
                ),
              ),

            )


          ],
        ),
      ),
    );
  }
}
