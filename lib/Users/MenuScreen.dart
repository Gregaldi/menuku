import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:line_icons/line_icons.dart';
import 'package:menuku/Component/alert.dart';
import 'package:menuku/Component/alert_style.dart';
import 'package:menuku/Component/constants.dart';
import 'package:menuku/Component/dialog_button.dart';
import 'package:menuku/Model/DetailModelResto.dart';
import 'package:menuku/Model/ErrorResponse.dart';
import 'package:menuku/Model/GetCartModel.dart';
import 'package:menuku/Model/MenuModel.dart';
import 'package:menuku/Model/UserInformation.dart';
import 'package:menuku/Users/AddToBasket.dart';
import 'package:menuku/Users/CartsScreen.dart';
import 'package:menuku/Users/WelcomePage.dart';
import 'package:menuku/Users/root_app.dart';
import 'package:menuku/utils/Session.dart';
import 'package:menuku/utils/colors.dart';
import 'package:menuku/utils/styles.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MenuScreen extends StatefulWidget {

  final String  body, id;
  const MenuScreen({required this.body,required this.id});


  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  late String token;
  Getdetailresto? getdetailresto;
  MenuModel? menuModel;
  ErrorResponse? errorResponse;
  UserInformation? userInformation;
  List<GetCartModel>? getCartModel;

  _gettoken() async {
    var data = await getAccessToken();
    setState(() {
      token = data!.accessToken;
      print(" token b2b " + token);
    });
  }
  _getUserInformation() async {
    var data = await getUserInformation();
    setState(() {
      userInformation = data;
      print(" user " + json.encode(data));

    });
  }
  Future getcart() async {
    Map body = {
      "user_client_code": userInformation!.user.userClientCode
    };
    List<GetCartModel> country = <GetCartModel>[];
    var response = await http.post(
        Uri.parse("https://api.menuku.subredtis.com/api/v1/getCart"),
        headers: {
          "Authorization": "Bearer "+token,
          "content-type": "application/json"
        },
        body:json.encode(body)
    );
    Iterable list = json.decode(response.body);
    var temp = <GetCartModel>[];
    temp = list.map((model) => GetCartModel.fromJson(model)).toList();
    country.addAll(temp);
    return country;
  }


  @override
  void initState() {
    super.initState();
    _gettoken();
    _getUserInformation();
    getcart();
  }

  Future getresto() async {

    Dio dio;
    BaseOptions options = new BaseOptions(
      baseUrl: "https://api.dev.cekmenuku.com",
      receiveDataWhenStatusError: true,
      headers: {
        "Authorization": "Bearer "+token,
      },
      connectTimeout: 60*1000, // 60 seconds
      receiveTimeout: 60*1000,
    );

    dio = new Dio(options);

    FormData formData = new FormData.fromMap({
      "code_resto":widget.id
    });
    try {
      Response response = await dio.post("/api/v1/getDetailResto", data: formData);

      if(response.statusCode == 200){
        print(response.toString());
        Getdetailresto restoModel = Getdetailresto.fromJson(response.data);
        print("abc "+ json.encode(restoModel));
        return restoModel;

      }

    }on DioError  catch (ex) {
      if(ex.type == DioErrorType.connectTimeout){
        var alertStyle = AlertStyle(
          descStyle: TextStyle(fontSize: 21),
          isCloseButton: false,
          isOverlayTapDismiss: false,
          descTextAlign: TextAlign.center,
          animationDuration:
          Duration(milliseconds: 400),
          alertBorder: RoundedRectangleBorder(
            borderRadius:
            BorderRadius.circular(0.0),
            side: BorderSide(
              color: Colors.grey,
            ),
          ),
          titleStyle: TextStyle(
            color: Colors.red,
          ),
          alertAlignment: Alignment.center,
        );
        Alert(
          context: context,
          style: alertStyle,
          type: AlertType.error,
          title: "Connection Timeout",
          buttons: [
            DialogButton(
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop();

              },
              child: Text(
                "Oke",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17),
              ),
            ),
          ],).show();
        throw Exception("Connection Timeout Exception");

      }else{
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.clear();
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => WelcomePage()),
        );
      }

    }

  }
  Future getmenu() async {
    Dio dio;
    BaseOptions options = new BaseOptions(
      baseUrl: "https://api.dev.cekmenuku.com",

      receiveDataWhenStatusError: true,
      headers: {
        "Authorization": "Bearer "+token,
      },
      connectTimeout: 60*1000, // 60 seconds
      receiveTimeout: 60*1000,
    );

    dio = new Dio(options);

    FormData formData = new FormData.fromMap({
        "code": widget.body
    });
    try {
      Response response = await dio.post("/api/v1/getMenusBarcode", data: formData);

      if(response.statusCode == 200){
        if(response.data['status']== 200){
          MenuModel menuModel = MenuModel.fromJson(response.data);
          print("abc "+ json.encode(menuModel));
          return menuModel;
        }else{
          ErrorResponse errorResponse = ErrorResponse.fromJson(response.data);
          print(response.data['status'].toString());
          return errorResponse;

        }
      }

    }on DioError  catch (ex) {
      if(ex.type == DioErrorType.connectTimeout){
        var alertStyle = AlertStyle(
          descStyle: TextStyle(fontSize: 21),
          isCloseButton: false,
          isOverlayTapDismiss: false,
          descTextAlign: TextAlign.center,
          animationDuration:
          Duration(milliseconds: 400),
          alertBorder: RoundedRectangleBorder(
            borderRadius:
            BorderRadius.circular(0.0),
            side: BorderSide(
              color: Colors.grey,
            ),
          ),
          titleStyle: TextStyle(
            color: Colors.red,
          ),
          alertAlignment: Alignment.center,
        );
        Alert(
          context: context,
          style: alertStyle,
          type: AlertType.error,
          title: "Connection Timeout",
          buttons: [
            DialogButton(
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop();

              },
              child: Text(
                "Oke",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17),
              ),
            ),
          ],).show();
        throw Exception("Connection Timeout Exception");

      }
    }

  }

  @override
  Widget build(BuildContext context) {
    return
      WillPopScope(
          onWillPop: () async {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => RootApp()),
            );
            return true;
          },
        child:
      Scaffold(
      body: getBody(),
      bottomSheet: getFooter(),
    ));
  }

  Widget getFooter() {
    var size = MediaQuery.of(context).size;
    return Container(
      height: 80,
      width: size.width,
      decoration: BoxDecoration(
          color: white,
          border: Border(top: BorderSide(color: black.withOpacity(0.1)))),
      child: Padding(
        padding: const EdgeInsets.only(top: 15),
        child: Column(
          children: [
            Container(
                margin: EdgeInsets.only(top: 2),
                height:60 ,
                width: MediaQuery.of(context).size.width/1.1,
                child:
                FutureBuilder(
                    future: getcart(),
                    builder: (context, AsyncSnapshot snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          return Text('Press button to start.');
                        case ConnectionState.active:
                        case ConnectionState.waiting:
                          return
                            Center(
                                child: CupertinoActivityIndicator()
                            )
                          ;
                        case ConnectionState.done:
                          if(snapshot.hasData){
                            getCartModel = snapshot.data as List<GetCartModel>? ;
                            print(" data " +snapshot.data.toString());
                            Codec<String, String> stringToBase64 = utf8.fuse(base64);
                            final formatCurrency = new NumberFormat.currency(
                                locale: 'ID', symbol: "Rp.", decimalDigits: 0);
                            double sum = getCartModel!.map((expense) => expense.price).fold(0, (prev, amount) => prev + int.parse(amount));
                            return
                              RaisedButton(
                                shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                ),
                                color: Colors.green,
                                autofocus: true,
                                onPressed: () async {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => CartScreen()),
                                  );

                                },
                                child:
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [

                                    Text('${getCartModel!.length.toString()} Item - ${formatCurrency.format(sum)}',style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: 'Inter-Regular',
                                        fontWeight: FontWeight.normal,
                                        color: Colors.white
                                    )
                                    ),
                                    Text('ORDER PESANAN',style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: 'Inter-Regular',
                                        fontWeight: FontWeight.normal,
                                        color: Colors.white
                                    )
                                    ),
                                  ],
                                )


                              );
                          }
                          return  RaisedButton(
                            shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                            ),
                            color: Colors.grey,
                            autofocus: true,
                            onPressed: () async {


                            },
                            child:

                            Text("Silahkan Pesan",style: TextStyle(
                                fontSize: 14,
                                fontFamily: 'Inter-Regular',
                                fontWeight: FontWeight.normal,
                                color: Colors.white
                            )),

                          );


                      }
                    }
                ),

            ),
          ],
        ),
      ),
    );
  }

  Widget getBody() {
    var size = MediaQuery.of(context).size;
    return
      FutureBuilder(
          future: getresto(),
          builder: (context,AsyncSnapshot snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return Text('Press button to start.');
              case ConnectionState.active:
              case ConnectionState.waiting:
                return
                  Center(
                      child: CupertinoActivityIndicator()
                  )
                ;
              case ConnectionState.done:
                getdetailresto = snapshot.data as Getdetailresto?;
                Codec<String, String> stringToBase64 = utf8.fuse(base64);
                String encoded = stringToBase64.decode(getdetailresto!.data.pictures);

                return

                  SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 60),
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Container(
                                width: size.width,
                                height: 150,
                                child:  Image(
                                  image: NetworkImage(
                                    "https://apmin.dev.cekmenuku.com/$encoded",
                                  ),
                                  fit: BoxFit.cover,
                                  errorBuilder: (context, url, error) => Image.network('https://career.astra.co.id/static/media/image_not_available1.94c0c57d.png'),
                                ),
                              ),
                              SafeArea(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    IconButton(
                                      icon: Container(
                                        width: 50,
                                        height: 50,
                                        decoration: BoxDecoration(
                                            color: white, shape: BoxShape.circle),
                                        child: Center(
                                          child: Icon(
                                            Icons.arrow_back,
                                            size: 18,
                                          ),
                                        ),
                                      ),
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(builder: (context) => RootApp()),
                                        );
                                      },
                                    ),
                                    IconButton(
                                      icon: Container(
                                        width: 50,
                                        height: 50,
                                        decoration: BoxDecoration(
                                            color: white, shape: BoxShape.circle),
                                        child: Center(
                                          child: Icon(
                                            Icons.favorite_border,
                                            size: 18,
                                          ),
                                        ),
                                      ),
                                      onPressed: () {},
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 15, right: 15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  getdetailresto!.data.name,
                                  style: TextStyle(fontSize: 21, fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      width: size.width - 30,
                                      child: Text(
                                          getdetailresto!.data.category[0].desc,
                                        style: TextStyle(fontSize: 14, height: 1.3),
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Container(
                                  child: Row(
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                            color: textFieldColor,
                                            borderRadius: BorderRadius.circular(3)),
                                        child: Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: Icon(
                                            Icons.hourglass_bottom,
                                            color: primary,
                                            size: 16,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                            color: textFieldColor,
                                            borderRadius: BorderRadius.circular(3)),
                                        child: Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: Text(
                                            "40-50 Min",
                                            style: TextStyle(
                                              fontSize: 14,
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                            color: textFieldColor,
                                            borderRadius: BorderRadius.circular(3)),
                                        child: Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: Row(
                                            children: [
                                              Text(
                                                "4.7",
                                                style: TextStyle(
                                                  fontSize: 14,
                                                ),
                                              ),
                                              SizedBox(
                                                width: 3,
                                              ),
                                              Icon(
                                                Icons.star,
                                                color: primary,
                                                size: 17,
                                              ),
                                              Text(
                                                "(16)",
                                                style: TextStyle(
                                                  fontSize: 14,
                                                ),
                                              ),
                                              SizedBox(
                                                width: 3,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Divider(
                                  color: black.withOpacity(0.3),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  "Store Info",
                                  style: customContent,
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      width: (size.width) * 0.80,
                                      child: Row(
                                        children: [
                                          SvgPicture.asset(
                                            "assets/images/pin_icon.svg",
                                            width: 15,
                                            color: black.withOpacity(0.5),
                                          ),
                                          SizedBox(
                                            width: 8,
                                          ),
                                         Expanded(
                                             child:  Text(
                                           getdetailresto!.data.address,
                                           style: TextStyle(fontSize: 14),
                                         ))
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                        child:
                                        GestureDetector(
                                          onTap: (){
                                            showDialog(
                                                context: context,
                                                builder: (BuildContext context) {
                                                  return AlertDialog(
                                                      content:
                                                      Container(
                                                        height: 180.0,
                                                        width: 400.0,
                                                        child:
                                                        Stack(
                                                          overflow: Overflow.visible,
                                                          children: <Widget>[
                                                            Positioned(
                                                              right: -40.0,
                                                              top: -40.0,
                                                              child: InkResponse(
                                                                onTap: () {
                                                                  Navigator.of(context).pop();
                                                                },
                                                                child: CircleAvatar(
                                                                  child: Icon(Icons.close),
                                                                  backgroundColor: Colors.red,
                                                                ),
                                                              ),
                                                            ),
                                                            Column(
                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              children: [
                                                                Text(
                                                                  "Alamat: ",
                                                                  style: TextStyle(fontSize: 14),
                                                                ),
                                                                Text(
                                                                 getdetailresto!.data.address,
                                                                  style: TextStyle(fontSize: 14),
                                                                ),
                                                                SizedBox(height: 10,),
                                                                Text(
                                                                  "No Telepon Resto: ",
                                                                  style: TextStyle(fontSize: 14),
                                                                ),
                                                                Text(
                                                                  getdetailresto!.data.noTelp,
                                                                  style: TextStyle(fontSize: 14),
                                                                ),
                                                              ],
                                                            )
                                                          ],
                                                        ),

                                                      )
                                                  );
                                                });
                                          },
                                          child:
                                          Text(
                                            "More Info",
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: primary,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        )
                                    ),

                                  ],
                                ),
                                SizedBox(
                                  height: 15,
                                ),

                                Divider(
                                  color: black.withOpacity(0.3),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Menu',
                                          style: TextStyle(fontSize: 14),
                                        ),
                                        Icon(
                                          LineIcons.search,
                                          size: 25,
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 30),
                                    Text(
                                      "Packed For You",
                                      style: TextStyle(
                                          fontSize: 21, fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(height: 30),
                                    FutureBuilder(
                                  future: getmenu(),
                                  builder: (context,AsyncSnapshot snapshot) {
                                    switch (snapshot.connectionState) {
                                      case ConnectionState.none:
                                        return Text('Press button to start.');
                                      case ConnectionState.active:
                                      case ConnectionState.waiting:
                                        return
                                          Center(
                                              child: CupertinoActivityIndicator()
                                          )
                                        ;
                                      case ConnectionState.done:
                                       var body = json.encode(snapshot.data);
                                       Map result = json.decode(body);
                                       print("resilt " + result["status"].toString());

                                       if(result["status"] == 200){
                                         menuModel = snapshot.data as MenuModel?;
                                       }else{
                                         errorResponse = snapshot.data as ErrorResponse?;
                                       }

                                        Codec<String, String> stringToBase64 = utf8.fuse(base64);

                                        if(menuModel == null){
                                          return Container(
                                            child: Center(
                                              child: Text(
                                                errorResponse!.mssg.toString()
                                              ),
                                            ),
                                          );
                                        }else{
                                          return
                                            SingleChildScrollView(
                                                child:
                                                Column(
                                                  children: List.generate(menuModel!.data.length, (index) {
                                                    String encoded = stringToBase64.decode(menuModel!.data[index].pictures);
                                                    final formatCurrency = new NumberFormat.currency(
                                                        locale: 'ID', symbol: "Rp.", decimalDigits: 0);
                                                    return
                                                    GestureDetector(
                                                      onTap: (){
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(builder: (context) => AddToBasket(nama: menuModel!.data[index].name,
                                                              id: widget.id,body: widget.body,
                                                              menus_resto_code: menuModel!.data[index].menusRestoCode,
                                                              price: menuModel!.data[index].price, price_after_disc: menuModel!.data[index].priceAfterDisc,
                                                              desc: menuModel!.data[index].desc, disc_percent: menuModel!.data[index].discPercent,
                                                              disc_amount: menuModel!.data[index].discAmount, pictures: menuModel!.data[index].pictures)),
                                                        );
                                                      },
                                                      child:
                                                      Padding(
                                                        padding: const EdgeInsets.only(bottom: 40),
                                                        child: Row(
                                                          children: [
                                                            Container(
                                                                width: (size.width - 30) * 0.6,
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                  CrossAxisAlignment.start,
                                                                  children: [
                                                                    Text(
                                                                      menuModel!.data[index].name,
                                                                      style: TextStyle(
                                                                          fontSize: 16,
                                                                          height: 1.5,
                                                                          fontWeight: FontWeight.w600),
                                                                    ),
                                                                    SizedBox(
                                                                      height: 10,
                                                                    ),
                                                                    Text(
                                                                      menuModel!.data[index].desc,
                                                                      style: TextStyle(height: 1.3),
                                                                    ),
                                                                    SizedBox(
                                                                      height: 10,
                                                                    ),
                                                                    menuModel!.data[index].priceAfterDisc == ''?
                                                                    Text(
                                                                      formatCurrency.format(int.parse(menuModel!.data[index].price)),
                                                                      style: TextStyle(
                                                                        height: 1.3,
                                                                        fontWeight: FontWeight.w500,
                                                                      )
                                                                    )
                                                                        :
                                                                    Text(
                                                                      formatCurrency.format(int.parse(menuModel!.data[index].price)),
                                                                      style: TextStyle(
                                                                          height: 1.3,
                                                                          fontWeight: FontWeight.w500,
                                                                        decoration:
                                                                        TextDecoration.lineThrough,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                )),
                                                            Expanded(
                                                              child: Container(
                                                                height: 110,
                                                                child: Padding(
                                                                  padding: EdgeInsets.only(
                                                                      left: 20, top: 10, bottom: 10),
                                                                  child: Image(
                                                                    image: NetworkImage(
                                                                        "https://apmin.dev.cekmenuku.com/$encoded",
                                                                        ),
                                                                    fit: BoxFit.cover,
                                                                    errorBuilder: (context, url, error) => Image.network('https://career.astra.co.id/static/media/image_not_available1.94c0c57d.png'),
                                                                  ),
                                                                ),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    );
                                                  }),
                                                )
                                            );
                                        }


                                    }
                                  }
                              )

                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  );
            }
          }
      );
  }
}