import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:menuku/Component/alert.dart';
import 'package:menuku/Component/alert_style.dart';
import 'package:menuku/Component/constants.dart';
import 'package:menuku/Component/custom_slider.dart';
import 'package:menuku/Component/dialog_button.dart';
import 'package:menuku/Model/RestoModel.dart';
import 'package:menuku/Model/json.dart';
import 'package:menuku/Users/store_detail_page.dart';
import 'package:menuku/utils/Session.dart';
import 'package:menuku/utils/colors.dart';
import 'package:menuku/utils/styles.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int activeMenu = 0;
  late String token;
  GetRestoModel? restoModel;

  _gettoken() async {
    var data = await getAccessToken();
    setState(() {
      token = data!.accessToken;
      print(" token b2b " + token);
    });
  }

  @override
  void initState() {
    super.initState();
    _gettoken();
  }
  Future getresto() async {

    Dio dio;
    BaseOptions options = new BaseOptions(
      baseUrl: "https://api.dev.cekmenuku.com",
      receiveDataWhenStatusError: true,
      headers: {
        "Authorization": "Bearer "+token,
      },
      connectTimeout: 60*1000, // 60 seconds
      receiveTimeout: 60*1000,
    );

    dio = new Dio(options);

    FormData formData = new FormData.fromMap({
      "created":"asc",
      "take":10
    });
    try {
      Response response = await dio.post("/api/v1/getResto", data: formData);

      if(response.statusCode == 200){
        print(response.toString());
        GetRestoModel restoModel = GetRestoModel.fromJson(response.data);
        print("abc "+ json.encode(restoModel));
        return restoModel;

      }

    }on DioError  catch (ex) {
      if(ex.type == DioErrorType.connectTimeout){
        var alertStyle = AlertStyle(
          descStyle: TextStyle(fontSize: 21),
          isCloseButton: false,
          isOverlayTapDismiss: false,
          descTextAlign: TextAlign.center,
          animationDuration:
          Duration(milliseconds: 400),
          alertBorder: RoundedRectangleBorder(
            borderRadius:
            BorderRadius.circular(0.0),
            side: BorderSide(
              color: Colors.grey,
            ),
          ),
          titleStyle: TextStyle(
            color: Colors.red,
          ),
          alertAlignment: Alignment.center,
        );
        Alert(
          context: context,
          style: alertStyle,
          type: AlertType.error,
          title: "Connection Timeout",
          buttons: [
            DialogButton(
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop();

              },
              child: Text(
                "Oke",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17),
              ),
            ),
          ],).show();
        throw Exception("Connection Timeout Exception");

      }else if(ex.response!.statusCode == 500){

      }


    }

  }



  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: white,
      body:  ListView(
        shrinkWrap: true,
        children: [
          CustomSliderWidget(
            items: [
              "assets/images/slide_1.jpg",
              "assets/images/slide_2.jpg",
              "assets/images/slide_3.jpg"
            ],
          ),
          Container(
            width: size.width,
            decoration: BoxDecoration(color: textFieldColor),
            child: Padding(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Container(
                decoration: BoxDecoration(color: white),
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 15,
                    bottom: 15,
                  ),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Container(
                      margin: EdgeInsets.only(left: 30),
                      child: Row(
                          children: List.generate(categories.length, (index) {
                            return Padding(
                              padding: const EdgeInsets.only(right: 35),
                              child: Column(
                                children: [
                                  SvgPicture.asset(
                                    categories[index]['img'],
                                    width: 40,
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Text(
                                    categories[index]['name'],
                                    style: customParagraph,
                                  )
                                ],
                              ),
                            );
                          })),
                    ),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.only(left: 15, right: 15, bottom: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "More to explore",
                  style: customTitle,
                ),
                SizedBox(
                  height: 15,
                ),
                FutureBuilder(
                    future: getresto(),
                    builder: (context,AsyncSnapshot snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          return Text('Press button to start.');
                        case ConnectionState.active:
                        case ConnectionState.waiting:
                          return
                            Center(
                                child: CupertinoActivityIndicator()
                            )
                          ;
                        case ConnectionState.done:
                          if(snapshot.hasData){
                            restoModel = snapshot.data as GetRestoModel?;
                            Codec<String, String> stringToBase64 = utf8.fuse(base64);
                            return
                              SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  children: List.generate(restoModel!.data.length, (index) {
                                    String encoded = stringToBase64.decode(restoModel!.data[index].pictures);
                                    return Padding(
                                      padding: const EdgeInsets.only(right: 15),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Stack(
                                            children: [
                                              GestureDetector(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (_) => StoreDetailPage(
                                                            img: restoModel!.data[index].pictures,
                                                            name: restoModel!.data[index].name,
                                                            id:restoModel!.data[index].restoCode,
                                                            alamat:restoModel!.data[index].address ,
                                                            notlp: restoModel!.data[index].noTelp,
                                                          )));
                                                },
                                                child: Container(
                                                  width: size.width - 30,
                                                  height: 160,
                                                  child:
                                                  Image.network("https://apmin.dev.cekmenuku.com/$encoded"),
                                                ),
                                              ),
                                              Positioned(
                                                bottom: 15,
                                                right: 15,
                                                child: SvgPicture.asset(
                                                  exploreMenu[index]['is_liked']
                                                      ? "assets/images/loved_icon.svg"
                                                      : "assets/images/love_icon.svg",
                                                  width: 20,
                                                  color: white,
                                                ),
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            height: 15,
                                          ),
                                          Text(
                                            restoModel!.data[index].name,
                                            style: TextStyle(
                                                fontSize: 16, fontWeight: FontWeight.w400),
                                          ),
                                          SizedBox(
                                            height: 8,
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                "Sponsored",
                                                style: TextStyle(
                                                  fontSize: 14,
                                                ),
                                              ),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Icon(
                                                Icons.info,
                                                color: Colors.grey,
                                                size: 15,
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            height: 8,
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                "Restoran ${restoModel!.data[index].category[0].desc}",
                                                style: TextStyle(
                                                  fontSize: 14,
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 8,
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                decoration: BoxDecoration(
                                                    color: textFieldColor,
                                                    borderRadius: BorderRadius.circular(3)),
                                                child: Padding(
                                                  padding: const EdgeInsets.all(5.0),
                                                  child: Icon(
                                                    Icons.hourglass_bottom,
                                                    color: primary,
                                                    size: 16,
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 8,
                                              ),
                                              Container(
                                                decoration: BoxDecoration(
                                                    color: textFieldColor,
                                                    borderRadius: BorderRadius.circular(3)),
                                                child: Padding(
                                                  padding: const EdgeInsets.all(5.0),
                                                  child: Text(
                                                    exploreMenu[index]['time'],
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 8,
                                              ),
                                              Container(
                                                decoration: BoxDecoration(
                                                    color: textFieldColor,
                                                    borderRadius: BorderRadius.circular(3)),
                                                child: Padding(
                                                  padding: const EdgeInsets.all(5.0),
                                                  child: Row(
                                                    children: [
                                                      Text(
                                                        exploreMenu[index]['rate'],
                                                        style: TextStyle(
                                                          fontSize: 14,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 3,
                                                      ),
                                                      Icon(
                                                        Icons.star,
                                                        color: yellowStar,
                                                        size: 17,
                                                      ),
                                                      Text(
                                                        exploreMenu[index]['rate_number'],
                                                        style: TextStyle(
                                                          fontSize: 14,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 3,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    );
                                  }),
                                ),
                              );
                          }else{
                            return Container(
                              child: Center(
                                child: Text(
                                  "Tidak Ada Restoran Terdekat"
                                ),
                              ),
                            );
                          }


                      }
                    }
                ),
            ],
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            width: size.width,
            height: 10,
            decoration: BoxDecoration(color: textFieldColor),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.only(left: 15, right: 15, bottom: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Popular Near You",
                  style: customTitle,
                ),
                SizedBox(
                  height: 15,
                ),
                FutureBuilder(
                    future: getresto(),
                    builder: (context,AsyncSnapshot snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          return Text('Press button to start.');
                        case ConnectionState.active:
                        case ConnectionState.waiting:
                          return
                            Center(
                                child: CupertinoActivityIndicator()
                            )
                          ;
                        case ConnectionState.done:
                          if(snapshot.hasData){
                            restoModel = snapshot.data as GetRestoModel?;
                            Codec<String, String> stringToBase64 = utf8.fuse(base64);
                            return
                              SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  children: List.generate(restoModel!.data.length, (index) {
                                    String encoded = stringToBase64.decode(restoModel!.data[index].pictures);
                                    return Padding(
                                      padding: const EdgeInsets.only(right: 15),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Stack(
                                            children: [
                                              GestureDetector(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (_) => StoreDetailPage(
                                                            img: restoModel!.data[index].pictures,
                                                            name: restoModel!.data[index].name,
                                                            id:restoModel!.data[index].restoCode,
                                                            alamat:restoModel!.data[index].address ,
                                                            notlp: restoModel!.data[index].noTelp,
                                                          )));
                                                },
                                                child: Container(
                                                  width: size.width - 30,
                                                  height: 160,
                                                  child:
                                                  Image.network("https://apmin.dev.cekmenuku.com/$encoded"),
                                                ),
                                              ),
                                              Positioned(
                                                bottom: 15,
                                                right: 15,
                                                child: SvgPicture.asset(
                                                  exploreMenu[index]['is_liked']
                                                      ? "assets/images/loved_icon.svg"
                                                      : "assets/images/love_icon.svg",
                                                  width: 20,
                                                  color: white,
                                                ),
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            height: 15,
                                          ),
                                          Text(
                                            restoModel!.data[index].name,
                                            style: TextStyle(
                                                fontSize: 16, fontWeight: FontWeight.w400),
                                          ),
                                          SizedBox(
                                            height: 8,
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                "Sponsored",
                                                style: TextStyle(
                                                  fontSize: 14,
                                                ),
                                              ),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Icon(
                                                Icons.info,
                                                color: Colors.grey,
                                                size: 15,
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            height: 8,
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                "Restoran ${restoModel!.data[index].category[0].desc}",
                                                style: TextStyle(
                                                  fontSize: 14,
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 8,
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                decoration: BoxDecoration(
                                                    color: textFieldColor,
                                                    borderRadius: BorderRadius.circular(3)),
                                                child: Padding(
                                                  padding: const EdgeInsets.all(5.0),
                                                  child: Icon(
                                                    Icons.hourglass_bottom,
                                                    color: primary,
                                                    size: 16,
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 8,
                                              ),
                                              Container(
                                                decoration: BoxDecoration(
                                                    color: textFieldColor,
                                                    borderRadius: BorderRadius.circular(3)),
                                                child: Padding(
                                                  padding: const EdgeInsets.all(5.0),
                                                  child: Text(
                                                    exploreMenu[index]['time'],
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 8,
                                              ),
                                              Container(
                                                decoration: BoxDecoration(
                                                    color: textFieldColor,
                                                    borderRadius: BorderRadius.circular(3)),
                                                child: Padding(
                                                  padding: const EdgeInsets.all(5.0),
                                                  child: Row(
                                                    children: [
                                                      Text(
                                                        exploreMenu[index]['rate'],
                                                        style: TextStyle(
                                                          fontSize: 14,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 3,
                                                      ),
                                                      Icon(
                                                        Icons.star,
                                                        color: yellowStar,
                                                        size: 17,
                                                      ),
                                                      Text(
                                                        exploreMenu[index]['rate_number'],
                                                        style: TextStyle(
                                                          fontSize: 14,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 3,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    );
                                  }),
                                ),
                              );
                          }else{
                            return Container(
                              child: Center(
                                child: Text(
                                    "Tidak Ada Restoran Terdekat"
                                ),
                              ),
                            );
                          }


                      }
                    }
                ),

              ],
            ),
          )
        ],
      ),
    );
  }

}
