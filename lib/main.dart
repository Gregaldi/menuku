import 'package:flutter/material.dart';
import 'package:menuku/Introduction.dart';
import 'package:menuku/SplashScreen.dart';

var routes = <String, WidgetBuilder>{

};



void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return
      MaterialApp(
        debugShowCheckedModeBanner: false,
        home:
        FutureBuilder(
          future: Init.instance.initialize(),
          builder: (context, AsyncSnapshot snapshot){
            // Show splash screen while waiting for app resources to load:
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const MaterialApp(
                  debugShowCheckedModeBanner: false,
                  home: SplashScreen());
            } else {
              return const MaterialApp(
                  debugShowCheckedModeBanner: false,
                  home: IntroScreen());
            }
          },
        ),
      );
  }
}

class Init {
  Init._();
  static final instance = Init._();
  Future initialize() async {
    // This is where you can initialize the resources needed by your app while
    // the splash screen is displayed.  Remove the following example because
    // delaying the user experience is a bad design practice!
    await Future.delayed(const Duration(seconds: 3));
  }
}