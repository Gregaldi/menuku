import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:menuku/Component/introduction_screen.dart';
import 'package:menuku/Model/page_view_model.dart';
import 'package:menuku/Users/WelcomePage.dart';

class IntroScreen extends StatefulWidget {
  const IntroScreen({Key? key}) : super(key: key);

  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  @override
  void initState(){
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }
  @override
  Widget build(BuildContext context) {
    return IntroductionScreen(
      pages: [
        PageViewModel(
          title: "Nearby restaurants",
          body: "You don't have to go far to find a good restaurant,we have provided all the restaurants that is near you",
          image: Image.asset("assets/intro1.png",scale: 4,),
        ),
        PageViewModel(
          title: "Select the Favorites Menu",
          body: "Now eat well, don't leave the house,You can choose your favorite food only with one click",
          image: Image.asset("assets/intro2.png",scale: 5,),
        ),
        PageViewModel(
          title: "Good food at a cheap price",
          body: "You can eat at expensive restaurants with affordable price",
          image: Image.asset("assets/intro3.png",scale: 5,),
        ),
      ],
      onDone: () {
        // When done button is press
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => WelcomePage()),
        );
      },
      showSkipButton: false,
      skip: const Icon(Icons.navigate_before),
      next: const Icon(Icons.navigate_next),
      done: const Text("Done", style: TextStyle(fontWeight: FontWeight.w600)),
      dotsDecorator: DotsDecorator(
          size: const Size.square(10.0),
          activeSize: const Size(20.0, 10.0),
          activeColor: Colors.brown,
          color: Colors.black26,
          spacing: const EdgeInsets.symmetric(horizontal: 3.0),
          activeShape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25.0)
          )
      ),
    );
  }
}
